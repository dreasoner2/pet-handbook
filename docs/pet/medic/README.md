---
forceTheme: purple
---
# Medical Handbook
<center>
Select a subdivision
</center>
<div class="row">
  <div class="column">
    <a href="../medic/">
      <center>
      <img src="https://t6.rbxcdn.com/b0bdf6de15beceba3b8a3502214d59ed"
        style="border-radius: 50%;">
      
   <p>Medical Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="../hazmat/">
      <center>
      <img src="https://t5.rbxcdn.com/677f0db1b57fedc0502bfeafd774acd6"
        style="border-radius: 50%;">
      
   <p>Hazmat Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="../fire/">
      <center>
      <img src="https://t2.rbxcdn.com/a796198d3e727715beb0158d5249bc45"
        style="border-radius: 50%;">
      
   <p>Fire Team</p>
      </center>
    </a>
  </div>
</div>


## Medical Team Rules
::: warning NOTE:
This is the handbook for the subsection of the Pinewood Emergency Team
**MEDICAL TEAM**

Please don't forget that is made for the medic team only, so no rules that are stated here apply on the other sub-divisions. 
Only if stated in their respective handbooks. 
:::

### (MT. 1) Always have a med kit
Medical Team is required to carry a med kit at all times.

### (MT. 2) Heal everyone (Exept enemies)
Medical Team are required to heal everyone they see whom are hurt, PBST members get healing priority. PET is not to heal enemies (ex. TMS, Raiders). 

### (MT. 3) No combat healing
Medical Team is not permitted to heal someone while they are in combat for 3 reasons.
#### (MT. 3A) No healing if dead
You cannot heal someone if you are dead. 
#### (MT. 3B) Unfair PVP
This makes it unfair for the one that person is fighting. 
#### (MT. 3C) May heal enemie
You may end up healing the one who you do not want to heal. 

## Medical Team Combat Medic Sub-Division



### (MT.4) PVP Heal
This is a Exemption to **MT.3**
You are allowed to combat and heal during combat as a Combat Medic. 

### (MT.4A) BE CAREFULL
Must be done carefully, don’t heal the enemy team.