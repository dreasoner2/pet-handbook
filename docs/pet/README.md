---
forceTheme: green
---
# PET Handbook 
Welcome to the Pinewood Emergency Team Handbook! This handbook outlines rules, frequently asked questions, our promotion system, and subdivision information.   
The purpose of PET is to keep players of Pinewood Builders Facilities safe.   
PET (Pinewood Emergency Team) is split into 3 subdivisions these being Fire, Hazmat, and Medical each ran by an assigned Chief.   
Any question regarding the following information can be sent to a Division Trainer, Chief, or Manager.  

## PET Leadership: 

- [Diddleshot](https://www.roblox.com/users/390939/profile): Owner of Pinewood Builders 
- [Irreflexive](https://www.roblox.com/users/51691706/profile): PET Director 
- [Dannycrew106](https://www.roblox.com/users/47371002/profile): PET Manager 
- [Csdi](https://www.roblox.com/users/33093423/profile): PET Manager 
 
## Current Team Chiefs:

- [Eve_Cll](https://www.roblox.com/users/463364519/profile) - Medical Team Chief 
- [Neal_Forest](https://www.roblox.com/users/288096737/profile) - Hazmat Team Chief 
- [Wizertex](https://www.roblox.com/users/173123833/profile) - Fire Team Chief 

<center>
Select a subdivision
</center>
<div class="row">
  <div class="column">
    <a href="medic/">
      <center>
      <img src="https://t6.rbxcdn.com/b0bdf6de15beceba3b8a3502214d59ed"
        style="border-radius: 50%;">
      
   <p>Medical Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="hazmat/">
      <center>
      <img src="https://t5.rbxcdn.com/677f0db1b57fedc0502bfeafd774acd6"
        style="border-radius: 50%;">
      
   <p>Hazmat Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="fire/">
      <center>
      <img src="https://t2.rbxcdn.com/a796198d3e727715beb0158d5249bc45"
        style="border-radius: 50%;">
      
   <p>Fire Team</p>
      </center>
    </a>
  </div>
</div>




## General Rules
If any of these rules are broken you will be given a consequence at the Trainer’s discretion

### (AM. 1) Don't touch core control's  
All members patrolling as PET are not allowed to touch any core controls or cooling systems. The only exception to this is the Hazmat Team, read HT.3/HT.4 for more details.

### (AM. 2) No PBST Weapons on duty
Any members patrolling as PET are not allowed to carry any form of PBST, TMS, or Target weapons. 
The only weapons allowed are OP weapons or credit weapons. TMS is also not allowed to carry PET weapons whilst on duty as TMS.

### (AM. 3) No multiple ranking
PET members may be in all subdivisions if they wish, however one subdivision does not have authority over another subdivision. 
Member’s rank in the main PET group does not dictate the amount of power you have in a subgroup.

### (AM. 4) Work together
All teams are to coordinate together and to act as one. If any issues are to arise a Trainer is to be called onto the server to resolve the issue.

### (AM. 5) No shenanigans
Members of PET are strictly forbidden from becoming mutants, starting fires, or causing any sort of mayhem.

### (AM. 6) How to be on-duty
To be on duty as PET you must wear their team’s uniform and have their ranktag set to PET.

- ``!ranktag on``
- ``!setgroup PET``

Free uniforms are provided at PBCC. Go out of the lobby, get down the elevator, and walk to the left untill you see the PET HQ

### (AM. 7) Do NOT switch in a raid or patrol 
Members of PET are prohibited from switching teams in the middle of a patrol or raid. You must stick to one team for that activity.

### (AM. 7) Always listen to trainers
All members of PET are to listen to their Trainer’s instructions at all times.


## Trainers and Chiefs Rules

### (TC. 1) Have a good training schedule
Trainers and Chiefs must figure out their own times to host a trainings so that we don’t collide with PBST, and they don’t collide with us.

### (TC. 2) 1 Hours between PET trainings
Trainers and Chiefs have to follow the 30 minute Training Policy, which means there cannot be a training 30 minutes after a previous training. This only applies to PET trainings, PBST trainings do not comply with this rule.

### (TC. 3) Use the PET-Server
Trainers and Chiefs have to utilize the ``!petserver`` for trainings if at a PB facility.

### (TC. 4) Use UTC as training time
Trainers should use UTC to schedule trainings. However, using EST as an extra display time is also allowed.

## Manager Rules
### (MS. 1) Host training for anyone
Managers are allowed to host trainings for all teams.

### (MS. 2) Exeption to 1h rule
Managers also need to follow the 30 minutes Training Policy, except they can override an SD PBST training. PBST is exempt from this rule. Only applies to PET.

## FAQ

 1. Are we allowed to carry weapons as PET?  
 You are allowed to carry Credit Weapons or any weapons from a gamepass.

 2. When are trainings hosted?  
 Trainings are hosted at random times, you can check the schedule in game at the PBDSF or run ``k!schedule`` pet in ``#bot-commands`` in the [PET Discord](https://discord.gg/t4KBPkM).

 3. Are we allowed to use the E-Coolant feature?  
Yes, you are allowed to use the E-Coolant if you must save the server. [Refer to this video by **Coasterteam** to see how it works.](https://www.youtube.com/watch?v=nskMAcMYE9M&feature=youtu.be)


::: warning NOTE:
These rules can be changed at any time. So please check every now and then for a update on the handbook.
:::
